﻿using System;
using System.Linq;
using System.Text;

namespace ASCIIconversion
{
    class Program
    {
        int[] CheckIfAnyPrimary(int[] values)
        {
            int count = 0;
            for(int i = 0; i < values.Length; i++)
            {
                count = 0;
                for(int j = 2; j < (values[i] / 2) + 1; j++)
                {
                    if (values[i] % j == 0)
                    {
                        count++;
                    }
                }
                if (count == 0)
                {
                    values[i]++;
                }
            }
            return values;
        }
        int[] CalculateAverage(int[] values)
        {
            for(int i = 0; i < values.Length-1; i++)
            {
                values[i] = (values[i] + values[i + 1]) / 2;
            }
            return values;
        }

        static void Main(string[] args)
        {

            Program program = new Program();
            Console.WriteLine("Enter a string  : ");
            string inputInString = Console.ReadLine();

            byte[] inputInBytes = Encoding.ASCII.GetBytes(inputInString);

            int[] inputInInt = inputInBytes.Select(x => (int)x).ToArray();

            int[] averageOfInput=program.CalculateAverage(inputInInt);

            for(int i=0; i < averageOfInput.Length - 1; i++)
            {
                Console.Write(averageOfInput[i]+" ");
            }
            Console.WriteLine();

            int[] averageAfterChecking = program.CheckIfAnyPrimary(averageOfInput);


            for (int i = 0; i < averageAfterChecking.Length - 1; i++)
            {
                Console.Write(averageAfterChecking[i] + " ");
            }
            Console.WriteLine();

            for (int i = 0; i < averageAfterChecking.Length - 1; i++)
            {
                Console.Write((char)averageAfterChecking[i] + " ");
            }
            Console.WriteLine();
            Console.ReadKey();
        }
    }
}
