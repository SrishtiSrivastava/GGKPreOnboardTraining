﻿using System;

namespace Binary_Search_Tree
{
    class Program
    {
        class Node
        {
            public int value;
            public Node right;
            public Node left;

        }
        class BinarySearchTree
        {
            public Node root;

            void AscendingOrder(Node node)
            {
                if (node != null)
                {
                    AscendingOrder(node.left);
                    Console.Write(node.value + " ");
                    AscendingOrder(node.right);
                }
            }

            void DescendingOrder(Node node)
            {
                if (node != null)
                {
                    DescendingOrder(node.right);
                    Console.Write(node.value + " ");
                    DescendingOrder(node.left);
                }
            }

            bool Search(Node root, int itemToBeSearched)
            {
                if (root == null)
                {
                    return false;
                }
                else if (root.value == itemToBeSearched)
                {
                    return true;
                }
                else if (root.value < itemToBeSearched)
                {
                    return Search(root.right, itemToBeSearched);
                }
                else if (root.value > itemToBeSearched)
                {
                    return Search(root.left, itemToBeSearched);
                }
                else
                {
                    return false;
                }
            }

            Node FindReplacementNode(Node node)
            {
                Node replacement = node;
                if (replacement.right != null)
                {
                    replacement = replacement.right;
                    while (replacement.left != null)
                    {
                        replacement = replacement.left;
                    }
                }

                if (replacement == node.right)
                {
                    replacement = node.left;
                    while (replacement.right != null)
                    {
                        replacement = replacement.right;
                    }
                }

                return replacement;
            }

            Node Delete(int elementToBeDeleted)
            {
                Node parent = root, current = root;
                bool isLeftChild = true;

                while (current.value != elementToBeDeleted)
                {
                    parent = current;
                    if (elementToBeDeleted > current.value)
                    {
                        current = current.right;
                        isLeftChild = false;
                    }
                    else
                    {
                        current = current.left;
                        isLeftChild = true;
                    }
                }
                if (current.value == elementToBeDeleted)
                {
                    //leaf nodes
                    if (current.right == null && current.left == null)
                    {
                        if (isLeftChild == true)
                        {
                            parent.left = null;
                        }
                        else if (isLeftChild == false)
                        {
                            parent.right = null;
                        }
                        else if (current == root)
                        {
                            root = null;
                        }
                    }
                    //1 child node
                    else if (current.left == null)
                    {
                        if (isLeftChild == false)
                        {
                            parent.right = current.right;
                        }
                        else
                        {
                            parent.left = current.right;
                        }
                    }
                    else if (current.right == null)
                    {
                        if (isLeftChild == false)
                        {
                            parent.right = current.left;
                        }
                        else
                        {
                            parent.left = current.left;
                        }
                    }
                    //2 children node
                    else
                    {
                        Node lagest = FindReplacementNode(current);
                        int largestValue = lagest.value;
                        root = Delete(lagest.value);
                        if (current == root)
                        {
                            root.value = largestValue;
                        }
                        else if (isLeftChild == true)
                        {
                            parent.left.value = largestValue;

                        }
                        else
                        {
                            parent.right.value = largestValue;
                        }
                    }
                }
                return root;
            }

            public void InsertNode(int nodeValue)
            {
                bool valueAdded = false;
                Node nodeToBeAdded = new Node();
                nodeToBeAdded.value = nodeValue;
                if (root == null)
                    root = nodeToBeAdded;
                else
                {
                    Node current = root;
                    Node parent;
                    while (!valueAdded)
                    {
                        parent = current;
                        if (nodeValue < current.value)
                        {
                            current = current.left;
                            if (current == null)
                            {
                                parent.left = nodeToBeAdded;
                                valueAdded = true;
                            }
                        }
                        else
                        {
                            current = current.right;
                            if (current == null)
                            {
                                parent.right = nodeToBeAdded;
                                valueAdded = true;
                            }
                        }
                    }
                }
            }
            static void Main(string[] args)
            {
                BinarySearchTree binarySearchTree = new BinarySearchTree();

                label: Console.WriteLine("MENU\n" +
                    "------------------------------------------------------------\n" +
                    "1.Enter values into the binary search tree\n" +
                    "2.View the content of binary search tree in Ascending order\n" +
                    "3.View content of binary search tree in Descending order\n" +
                    "4.Search a number in the binary search tree\n" +
                    "5.Delete a number from the tree\n" +
                    "\n------------------------------------------------------------" +
                    "\nEnter your choice( 1/ 2/ 3/ 4/ 5 ) : ");
                int choice = int.Parse(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        Console.WriteLine("Enter the number of elements to be added to the tree : ");
                        int numberOfElements = int.Parse(Console.ReadLine());
                        for (int i = 0; i < numberOfElements; i++)
                        {
                            Console.WriteLine("Enter the number to be added :");
                            int number = int.Parse(Console.ReadLine());
                            if (binarySearchTree.Search(binarySearchTree.root, number) == false)
                            {
                                binarySearchTree.InsertNode(number);
                            }
                        }
                        break;
                    case 2:
                        if (binarySearchTree.root != null)
                        {
                            Console.WriteLine("The content of the tree in ascending order is :");
                            binarySearchTree.AscendingOrder(binarySearchTree.root);
                            Console.WriteLine();
                        }
                        else
                        {
                            Console.WriteLine("Binary search tree is empty.");
                            Console.WriteLine();
                        }
                        break;

                    case 3:
                        if (binarySearchTree.root != null)
                        {
                            Console.WriteLine("The content of the tree in descending order is :");
                            binarySearchTree.DescendingOrder(binarySearchTree.root);
                            Console.WriteLine();
                        }
                        else
                        {
                            Console.WriteLine("Binary search tree is empty.");
                            Console.WriteLine();
                        }

                        break;

                    case 4:
                        Console.WriteLine("Enter the number to be searched : ");
                        int input = int.Parse(Console.ReadLine());
                        if (binarySearchTree.Search(binarySearchTree.root, input) == true)
                        {
                            Console.WriteLine(input + " present in the tree");
                        }
                        else
                        {
                            Console.WriteLine(input + " not present in the tree");
                        }
                        break;
                    case 5:
                        Console.WriteLine("Enter the number to be deleted : ");
                        int num = int.Parse(Console.ReadLine());
                        if (binarySearchTree.Search(binarySearchTree.root, num) == true)
                        {
                            binarySearchTree.root = binarySearchTree.Delete(num);
                            Console.WriteLine("Element deleted.\nContent of binary tree after deletion (ascending order) :\n");
                            binarySearchTree.AscendingOrder(binarySearchTree.root);
                        }
                        else
                        {
                            Console.WriteLine("Number not present in the tree.");
                            Console.WriteLine();
                        }
                        break;

                    default:
                        Console.WriteLine("Wrong input! Try again");
                        goto label;
                }
                Console.WriteLine("\n-------------------------------------------------" +
                                  "\nDo you want to perform any other operation?(y/n) : ");
                if ('y' == char.Parse(Console.ReadLine()))
                    goto label;
            }
        }
    }
}
