﻿using System;
using System.Collections.Generic;

namespace GenericStackAndQueue
{
    class Stack<T>
    {
        static T[] stack;
        static int topOfStack;
        static int maxStackSize;
        
        public Stack(int maxSizeOfStack)
        {
            topOfStack = -1;
            maxStackSize = maxSizeOfStack;
            stack = new T[maxSizeOfStack];
        }

        public bool push(T node)
        {
            
            if (topOfStack < maxStackSize - 1)
            {
                stack[++topOfStack] = node;
                return true;
            }
            else
                return false;
        }
        public void pop()
        {
            if (topOfStack > 0)
            {
                Console.WriteLine(stack[topOfStack--] + " popped");
            }
            else
            {
                Console.WriteLine("Stack underflow");
            }
               
        }

        public void showStack()
        {
            if (topOfStack == -1)
            {
                Console.WriteLine("Stack empty");
            }
            else
            {
                foreach (T t in stack)
                {
                    Console.Write(t + " ");
                }
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            int choice;

           mainChoice: Console.WriteLine("1.Stack\n2.Queue\nEnter your choice(1/2) : ");
            switch (int.Parse(Console.ReadLine()))
            {
                case 1:

                    Console.WriteLine("Enter the max size of stack : ");
                    Stack<string> myStack = new Stack<string>(int.Parse(Console.ReadLine()));
                    stackOps: Console.WriteLine("Stack\n1.Push\n2.Pop\n3.Display stack\nEnter your choice(1/2/3):");
                    choice = int.Parse(Console.ReadLine());
                    switch (choice)
                    {
                        case 1:
                            Console.WriteLine("Enter the value to be entered : ");
                            if (myStack.push(Console.ReadLine()))
                            {
                                Console.WriteLine("Value pushed");
                            }
                            else
                            {
                                Console.WriteLine("Stack overflow");
                            }
                            break;
                        case 2:
                            myStack.pop();
                            break;
                        case 3:
                            Console.WriteLine("Content of stack : \n");
                            myStack.showStack();
                            Console.WriteLine();
                            break;
                        default:
                            Console.WriteLine("Wrong choice!\nTry again!\n");
                            goto stackOps;

                    }
                    Console.WriteLine("Do you want to perform any operation again? (y/n): ");
                    if (char.Parse(Console.ReadLine()) == 'y')
                    {
                        goto stackOps;
                    }
                    break;

                case 2:
                    Queue<string> myQueue = new Queue<string>();

                   queueOps: Console.WriteLine("Queue\n1.Enter an element\n2.Delete an element\n3.Display all elements\nEnter your choice(1/2/3) : ");
                    switch(int.Parse(Console.ReadLine()))
                    {
                        case 1:
                            Console.WriteLine("Enter the element to be added : ");
                            myQueue.Enqueue(Console.ReadLine());
                            break;
                        case 2:
                            Console.WriteLine(myQueue.Dequeue() + " removed.");
                            break;
                        case 3:
                            foreach (string s in myQueue)
                            {
                                Console.WriteLine(s);
                            }
                            break;
                        default:
                            Console.WriteLine("Wrong choice!\nTry again!\n");
                            goto queueOps;
                    }
                    Console.WriteLine("Do you want to perform any operation again? (y/n): ");
                    if (char.Parse(Console.ReadLine()) == 'y')
                    {
                        goto queueOps;
                    }
                    break;

                default:
                    Console.WriteLine("Wrong choice!\nTry again!\n");
                    goto mainChoice;

            }
        }
    }
}
