﻿using System;

namespace AddingTwoNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            string num1, num2;
            do {
                Console.WriteLine("Enter 2 numbers : ");
                num1 = Console.ReadLine();
                num2 = Console.ReadLine();
                decimal number1, number2, sum;

                decimal.TryParse(num1, out number1);
                decimal.TryParse(num2, out number2);

                if (number1 != 0 && number2 != 0)
                {
                    try
                    {
                        sum = number1 + number2;
                        Console.WriteLine("Sum :" + sum);

                    }
                    catch
                    {
                        number1 = 0;
                        goto sumGreaterThanRange;
                    }
                }
                sumGreaterThanRange: if (number1 == 0 || number2 == 0)
                {
                    if (num1.Length > num2.Length)
                    {

                        number1 = decimal.Parse(num1.Substring(0, 29));

                        int lengthReduced = num1.Length - number1.ToString().Length;

                        if (num2.Length >= lengthReduced)
                        {
                            number2 = decimal.Parse(num2.Substring(0, num2.Length - lengthReduced));
                            sum = number1 + number2;
                            Console.WriteLine("Sum :" + sum.ToString().Substring(0, 1) + "."
                            + sum.ToString().Substring(1, 8) + "E+" + (num1.Length - 1));

                        }
                        else
                            Console.WriteLine("Sum :" + number1.ToString().Substring(0, 1) + "."
                         + number1.ToString().Substring(1, 8) + "E+" + (num1.Length - 1));

                    }
                    if (num1.Length < num2.Length)
                    {
                        number2 = decimal.Parse(num2.Substring(0, 29));

                        int lengthReduced = num2.Length - number2.ToString().Length;

                        if (num1.Length >= lengthReduced)
                        {
                            number1 = decimal.Parse(num1.Substring(0, num1.Length - lengthReduced));
                            sum = number1 + number2;
                            Console.Write("Sum :" + sum.ToString().Substring(0, 1) + "."
                        + sum.ToString().Substring(1, 8) + "E+" + (num1.Length - 1));

                        }
                        else
                            Console.Write("Sum :" + number2.ToString().Substring(0, 1) + "."
                         + number2.ToString().Substring(1, 8) + "E+" + (num1.Length - 1));
                    }
                }
                if (num1.Length == num2.Length && number1 == 0 && number2 == 0)
                {
                    number1 = decimal.Parse(num1.Substring(0, 28));
                    number2 = decimal.Parse(num2.Substring(0, 28));
                    sum = number1 + number2;

                    Console.Write("Sum :" + sum.ToString().Substring(0, 1) + "."
                        + sum.ToString().Substring(1, 8) + "E+" + (num1.Length - 1));
                }
                Console.WriteLine("Do you want to perform more additions? (y / n) :");
            } while (char.Parse(Console.ReadLine()) == 'y');
        }
    }
}