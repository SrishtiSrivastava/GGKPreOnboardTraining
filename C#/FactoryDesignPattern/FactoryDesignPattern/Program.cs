﻿using System;

namespace FactoryDesignPattern
{
    abstract class IVehicle
    {
        public abstract void Build();

    }
    class Bus : IVehicle
    {
        public override void Build()
        {
            Console.WriteLine("Class : Bus\nFunction called : Build()");
        }
    }
    class Car : IVehicle
    {
        public override void Build()
        {
            Console.WriteLine("Class : Car\nFunction called : Build()");
        }
    }
    class Truck : IVehicle
    {
        public override void Build()
        {
            Console.WriteLine("Class : Truck\nFunction called :Build()");
        }
    }

    class Factory
    {
        public void BuildVehicles()
        {
            int type = 0;
            char choice;
            IVehicle vehicle;
            do
            {
                Console.WriteLine("Vehicle type\n1.Bus\n2.Car\n3.Truck\nEnter your choice(1/2/3) : ");
                type = int.Parse(Console.ReadLine());
                switch (type)
                {
                    case 1:
                        vehicle = new Bus();
                        vehicle.Build();
                        break;
                    case 2:
                        vehicle = new Car();
                        vehicle.Build();

                        break;
                    case 3:
                        vehicle = new Truck();
                        vehicle.Build();

                        break;
                }
                Console.WriteLine("Do you want to perform any other operation? (y/n) : ");
            } while (char.Parse(Console.ReadLine()) == 'y');
        }
        static void Main(string[] args)
        {
            Factory factory = new Factory();
            factory.BuildVehicles();
        }
    }
}
