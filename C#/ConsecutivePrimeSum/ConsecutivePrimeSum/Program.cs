﻿using System;

namespace ConsecutivePrimeSum
{
    class Program
    {
        public bool CheckIfPrime(int number)
        {
            int i, numberOfTimesDivided = 0;

            for (i = 2; i < number / 2 + 1; i++)
            {
                if (number % i == 0)
                {
                    numberOfTimesDivided++;
                }
            }
            if (numberOfTimesDivided == 0)
            {
                return true;
            }
            else
                return false;
        }
        public bool SumOfPrimeNumber(int primeNumber)
        {
            int i, sum = 0;

            for (i = 2; i < primeNumber; i++)
            {
                if (CheckIfPrime(i) == true)
                {
                    sum += i;
                    if (sum >= primeNumber)
                        break;
                }
            }
            if (sum == primeNumber)
            {
                return true;
            }
            else
                return false;

        }
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a number : ");
            int num = int.Parse(Console.ReadLine());
            Program program = new Program();
            int countPrimeNumbers = 0;
            for (int i = 3; i < num; i++)
            {
                if (program.CheckIfPrime(i) == true)
                {
                    if (program.SumOfPrimeNumber(i) == true)
                    {
                        countPrimeNumbers++;
                    }
                }
            }
            Console.WriteLine("Prime numbers that can be expressed as sum of " +
                "other consecutive prime numbers till {0} are :\n{1}", num, countPrimeNumbers);
            Console.ReadKey();
        }
    }
    
}
