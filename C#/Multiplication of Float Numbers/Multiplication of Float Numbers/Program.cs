﻿using System;

namespace Multiplication_of_Float_Numbers
{
    class BinaryOperation
    {
        //Converting decimal to binary 

        public string FloatToBinary(decimal decimalNumber)
        {
            if (decimalNumber > 0)
            {
                int partBeforeTheDecimal = (int)decimalNumber;
                decimal partAfterTheDecimal = decimalNumber - partBeforeTheDecimal;
                return "0" +(ConversionOfPartBeforeTheDecimal(partBeforeTheDecimal) + ConversionOfPartAfterTheDecimal(partAfterTheDecimal)).ToString();
            }
            else
            {
                decimalNumber = decimalNumber * -1;
                int partBeforeTheDecimal = (int)decimalNumber;
                decimal partAfterTheDecimal = decimalNumber - partBeforeTheDecimal;
                return "1" + (ConversionOfPartBeforeTheDecimal(partBeforeTheDecimal) + ConversionOfPartAfterTheDecimal(partAfterTheDecimal)).ToString();
            }
        }

        protected decimal ReverseNumber(string binaryNumber)
        {
            decimal reversedNumber = 0;

            int size = binaryNumber.Length;

            for (int i = size - 1; i >= 0; i--)
            {
                reversedNumber = reversedNumber * 10 + int.Parse(binaryNumber[i].ToString());
            }

            return reversedNumber;
        }
        protected decimal ConversionOfPartBeforeTheDecimal(int partBeforeTheDecimal)
        {
            string binaryValue = String.Empty;
            while (partBeforeTheDecimal > 0)
            {
                string temp = (partBeforeTheDecimal % 2).ToString();
                partBeforeTheDecimal = partBeforeTheDecimal / 2;
                binaryValue += temp;
            }
            return ReverseNumber(binaryValue);
        }


        protected decimal ConversionOfPartAfterTheDecimal(decimal partAfterTheDecimal)
        {
            decimal temp, convertedBinary = 0;

            int size = 0;

            while (size < 5 && partAfterTheDecimal <= 1)
            {
                partAfterTheDecimal *= 2;

                if (partAfterTheDecimal >= 1)
                {
                    temp = (decimal)Math.Pow(10, -size - 1) * 1;
                    convertedBinary = convertedBinary + temp;
                    partAfterTheDecimal = partAfterTheDecimal - 1;
                }
                size++;
            }
            
            return convertedBinary;
        }


        //converting binary to decimal 

        public decimal BinaryToFloat(string number)
        {
            int sign = int.Parse(number.Substring(0,1));
            number = number.Substring(1);
            decimal binaryNumber = decimal.Parse(number);
            int i, size;
            string partBeforeTheDecimal;
            string partAfterTheDecimal;
            decimal temp;

            string[] binaryNumberAfterSplit = binaryNumber.ToString().Split('.');
            
            partBeforeTheDecimal = binaryNumberAfterSplit[0];
            try
            {
                partAfterTheDecimal = binaryNumberAfterSplit[1];
            }
            catch
            {
                partAfterTheDecimal = String.Empty;
            }

            decimal convertedDecimal = 0;

            size = partBeforeTheDecimal.Length;

            for (i = 0; i < size; i++)
            {
                temp = int.Parse(partBeforeTheDecimal[size - i - 1].ToString());
                convertedDecimal = convertedDecimal + temp * (int)Math.Pow(2, i);
            }

            size = partAfterTheDecimal.Length;
            
            temp = 0;
            for (i = 0; i < size; i++)
            {
                if (partAfterTheDecimal[i] == '1')
                {
                    temp += (1 / (decimal)Math.Pow(2, i + 1));
                }
            }

            convertedDecimal = convertedDecimal + temp;
            
            if (sign == 1)
            {
                convertedDecimal = convertedDecimal * -1;
            }
            return convertedDecimal;
        }
        //binary multiplication
        public string Multiply(string number1, string number2)
        {
            int sign1 = int.Parse(number1.Substring(0, 1));
            int sign2 = int.Parse(number2.Substring(0, 1));

            //removing sign bit 
            number1 = number1.Substring(1);
            number2 = number2.Substring(1);

            decimal binary1 = decimal.Parse(number1);
            decimal binary2 = decimal.Parse(number2);

            decimal product = 0;
            int power1 = 0, power2 = 0;
            PartAfterTheDecimal(ref binary1, ref power1);
            PartAfterTheDecimal(ref binary2, ref power2);

            int factor = 1;
            while (binary2 > 0)
            {
                if ((binary2 % 10) == 1)
                {
                    decimal temp = (decimal)(binary1 * factor);
                    product = AddBinaryNumbers(product.ToString(), temp.ToString());
                }
                try
                {
                    binary2 = (int)binary2 / 10;
                }
                catch
                {
                    binary2 = decimal.Parse(binary2.ToString().Substring(0, binary2.ToString().Length - 1));
                }
                factor = factor * 10;
                
            }
            if (power1 + power2 > 0)
            {
                PlaceDecimalPoint(ref product, power1 + power2);
            }

            if(sign1 != sign2)
            {
                //negative product
                return "1" + product.ToString();
            }
            else
            {
                //positive product
                return "0" + product.ToString();
            }
        }
        protected void PlaceDecimalPoint(ref decimal number, int power)
        {
            int size = number.ToString().Length;
            string changedNumber = String.Empty;
            changedNumber = number.ToString().Substring(0, size - power);
            changedNumber += ".";
            changedNumber += number.ToString().Substring(size - power);
            number = decimal.Parse(changedNumber);

        }
        protected void PartAfterTheDecimal(ref decimal number, ref int power)
        {
            int size = number.ToString().Length;

            int lengthBeforeDecimal = number.ToString().Split('.')[0].Length;
        
            if (lengthBeforeDecimal == number.ToString().Length)
            {
                return;
            }
            power = number.ToString().Length - lengthBeforeDecimal - 1;
            string temp = (number * (decimal)Math.Pow(10, power)).ToString().Substring(0, size);
            number = decimal.Parse(temp);
        }
        protected decimal AddBinaryNumbers(string number1, string number2)
        {
            if(number1 == "0")
            {
                return decimal.Parse(number2);
            }
            string sum = String.Empty;
         
            LengthCorrection(ref number1, ref number2);
            
            string rNumber1 = StringReverse(number1.ToString());

            string rNumber2 = StringReverse(number2.ToString());

            char currentNum1, currentNum2, carry = '0';

            for (int i = 0; i < rNumber1.Length; i++)
            {

                currentNum1 = rNumber1[i];
                currentNum2 = rNumber2[i];

                if (currentNum1 == '0' && currentNum2 == '0')
                {
                    if (carry == '0')
                    {
                        sum += "0";
                    }
                    else
                    {
                        sum += "1";
                        carry = '0';
                    }
                }
                else if ((currentNum1 == '1' && currentNum2 == '0') || (currentNum1 == '0' && currentNum2 == '1'))
                {
                    if (carry == '0')
                    {
                        sum += "1";
                    }
                    else
                    {
                        sum += "0";
                        carry = '1';
                    }
                }
                else if (currentNum1 == '1' && currentNum2 == '1')
                {
                    if (carry == '0')
                    {
                        sum += "0";
                        carry = '1';
                    }
                    else
                    {
                        sum += "1";
                        carry = '1';
                    }
                }
            }
            if(carry == '1')
            {
                sum += "1";
                carry = '0';
            }
            sum = StringReverse(sum);
            return decimal.Parse(sum);
        }
        //adding zeores at the beginning
        protected void LengthCorrection(ref string first, ref string second)
        {
            int lengthOfFirst = first.Length, lengthOfSecond = second.Length;

            if (lengthOfFirst == lengthOfSecond)
                return;
            else
            {
                if (lengthOfFirst > lengthOfSecond)
                {
                    while (lengthOfFirst != second.Length)
                    {
                        second = "0" + second;
                    }
                }
                else if (lengthOfFirst < lengthOfSecond)
                {
                    while (first.Length != lengthOfSecond)
                    {
                        first = "0" + first;
                    }
                }
            }
        }
        protected string StringReverse(string number)
        {
            string temp = String.Empty;
            for (int i = number.Length - 1; i >= 0; i--)
            {
                temp += number[i];
            }
            return temp;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            BinaryOperation binary = new BinaryOperation();
            decimal number1, number2;
            string binary1, binary2;

            char choice = 'n';

            do
            {
                Console.WriteLine("Enter 2 numbers : ");
                number1 = decimal.Parse(Console.ReadLine());
                number2 = decimal.Parse(Console.ReadLine());

                binary1 = binary.FloatToBinary(number1);
                binary2 = binary.FloatToBinary(number2);

                Console.WriteLine("Binary of these numbers :\n{0} {1}", binary1, binary2);

                string result = binary.Multiply(binary.FloatToBinary(number1), binary.FloatToBinary(number2));

                Console.WriteLine("The product of these binary numbers is : {0}", result);

                Console.WriteLine("{0} in float would be : {1}", result, binary.BinaryToFloat(result));

                Console.WriteLine("Do you want to perform this operation for any other number (y/n) : ");
                choice = char.Parse(Console.ReadLine());

            }
            while (choice == 'y');
        }
    }
}
