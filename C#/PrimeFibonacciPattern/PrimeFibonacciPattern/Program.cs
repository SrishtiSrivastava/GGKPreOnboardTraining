﻿using System;

namespace PrimeFibonacciPattern
{
    class Program
    {
        static private int index;
        public bool IsPrime(uint number)
        {
            uint i;
            byte count = 0;
            for (i = 2; i < number / 2; i++)
            {
                if (number % i == 0)
                {
                    count++;
                    break;
                }
            }
            if(count == 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public uint[] GetFibonacciSeries(byte limit)
        {
            uint a = 2, b = 3;
            byte count = 0;
            index = 0;
            uint[] fibonacciSeries = new uint[limit];
            while (count < limit) 
            {
                if (IsPrime(a) == true)
                {
                    fibonacciSeries[index++] = a;
                }
                if (b < limit && IsPrime(b) == true) 
                {
                    fibonacciSeries[index++] = b;
                }
                a = a + b;
                b = b + a;
                count = (byte)(count + 2);
            }
            return fibonacciSeries;
        }

        static void Main(string[] args)
        {
            Program program = new Program();
            Console.WriteLine("Input : ");
            byte input = byte.Parse(Console.ReadLine());

            uint[] fibonacciSeries = program.GetFibonacciSeries(input);

            for(byte i = 0; i < index + 1; i++)
            {
                for (byte j = 0; j < i; j++)
                {
                   Console.Write(fibonacciSeries[j] + " ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
