﻿using System;

namespace GenericBinarysearchTree
{
    class Program
    {
        public class Node<V> : IComparable
        {
            public V value;
            public Node<V> leftChild;
            public Node<V> rightChild;
            
            public int CompareTo(object obj)
            {
                return value.ToString().CompareTo(obj.ToString());
            }
        }

        class BinarySearchTree
        {

            public Node<string> root = new Node<string>();

            public bool Search(Node<string> node, string itemToBeSearched)
            {
                if (node == null)
                {
                    return false;
                }
                else if (itemToBeSearched.Equals(node.value))
                {
                    return true;
                }
                else if (itemToBeSearched.CompareTo(node.value) > 0)
                {
                    return Search(node.rightChild, itemToBeSearched);
                }
                else if (itemToBeSearched.CompareTo(node.value) < 0)
                {
                    return Search(node.leftChild, itemToBeSearched);
                }
                else
                {
                    return false;
                }
            }

            public void AscendingOrder(Node<string> node)
            {
                if (node != null)
                {
                    AscendingOrder(node.leftChild);
                    Console.Write(node.value + " ");
                    AscendingOrder(node.rightChild);
                }
            }
       
            public void InsertNode(string nodeValue)
            {
                bool valueAdded = false;
                Node<string> nodeToBeAdded = new Node<string>();
                nodeToBeAdded.value = nodeValue;
                if (root.value == null)
                    root = nodeToBeAdded;
                else
                {
                    Node<string> current = root;
                    Node<string> parent;
                    while (!valueAdded)
                    {
                        parent = current;
                        if (nodeValue.CompareTo(current.value) < 0)
                        {
                            current = current.leftChild;
                            if (current == null)
                            {
                                parent.leftChild = nodeToBeAdded;
                                valueAdded = true;
                            }
                        }
                        else
                        {
                            current = current.rightChild;
                            if (current == null)
                            {
                                parent.rightChild = nodeToBeAdded;
                                valueAdded = true;
                            }
                        }
                    }
                }
            }
            
        }

        static void Main(string[] args)
        {
            BinarySearchTree binarySearchTree = new BinarySearchTree();

         menu:   Console.WriteLine("1.Insert data\n2.Display data\n3.Search data\nEnter your choice (1/2/3) : ");
            int choice = int.Parse(Console.ReadLine());
            switch (choice)
            {
                case 1:
            Console.WriteLine("Enter the number of elements to be added to the tree : ");
            int numberOfElements = int.Parse(Console.ReadLine());
            for (int i = 0; i < numberOfElements; i++)
            {
                Console.WriteLine("Enter the data to be added :");
                string data = Console.ReadLine();
                binarySearchTree.InsertNode(data);
            }
                    break;
                case 2:
            Console.WriteLine("The content of the tree in ascending order is :");
            binarySearchTree.AscendingOrder(binarySearchTree.root);
                    Console.WriteLine();
                    break;
                case 3:
            Console.WriteLine("Enter the data to be searched : ");
            string input = Console.ReadLine();
            if (binarySearchTree.Search(binarySearchTree.root, input) == true)
            {
                Console.WriteLine(input + " present in the tree");
            }
            else
            {
                Console.WriteLine(input + " not present in the tree");
            }
                    break;
                default:Console.WriteLine("Incorrect option try again!");
                    goto menu;

        }
            Console.WriteLine("Do you want to perform any other operations? (y/n) :");
            if (char.Parse(Console.ReadLine()) == 'y')
            {
                goto menu;
            }
        }
    }
}
